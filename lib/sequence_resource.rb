require 'active_resource'

class SequenceResource < ActiveResource::Base
  self.site ='http://classyfire.wishartlab.com'
  self.element_name = "sequence"
end