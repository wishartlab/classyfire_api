require 'active_resource'
require 'rest-client'

class NodeResource < ActiveResource::Base
  self.site ='http://classyfire.wishartlab.com'
  self.element_name = "tax_node"
  has_many :entities, class_name: 'EntityResource'
end