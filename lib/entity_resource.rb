require 'active_resource'

class EntityResource < ActiveResource::Base
  self.site ='http://classyfire.wishartlab.com'
  self.element_name = "entity"
end