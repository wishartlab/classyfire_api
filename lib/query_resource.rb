require 'active_resource'

class QueryResource < ActiveResource::Base
  self.site ='http://classyfire.wishartlab.com'
  self.element_name = "query"
  has_many :entities, class_name: 'EntityResource'
  def to_param
    self.identifier
  end
end