[**ClassyFire**](http://classyfire.wishartlab.com) is a resource that allows you to automatically classify any chemical compound based on its structure. This is the repository for the ClassyFire API, which allows you to send requests to and pull information from the ClassyFire web server.

This ClassyFire API currently requires a version of the `rest-client` gem prior to v2. In order to use the commands below in a Ruby console, first run e.g.

    irb
    > gem 'rest-client', '=1.8.0'
    > require 'rest-client'
    > require 'path/to/classyfire_api.rb'


# **Endpoints** #

##1. Query##

The Query model represents the chemical structure to be classified. Its attributes are the query label and the query input.
The query label is an identifier

The query input contains the compound's identifier (optional) and its structural representation, separated by a tab. The chemical structure is represented either in the [Daylight SMILES](http://www.daylight.com/dayhtml/doc/theory/theory.smiles.html) format, the [InChI](http://www.iupac.org/home/publications/e-resources/inchi.html) format. Moreover, the compound can be represented with its [IUPAC name](http://www.chem.qmul.ac.uk/iupac/).
The input can also be of type [FASTA](https://en.wikipedia.org/wiki/FASTA_format), which contains peptide or nucleotide sequences.

###POST###
A query can be submitted using the POST method as demonstrated in the following example.

    ClassyFireAPI.submit_query("curl_test","MOL1\\tCCCOCC\\nMOL2\\tCOCC=CCCC","STRUCTURE")

where the parameters are:

* The query label: curl_test,
* The entity identifiers: MOL1,MOL2
* The entity's structure represented in the SMILES format: CCCOCC and COCC=CCCC.
* The query is of type 'STRUCTURE'

To submit multiple structures from a file, use e.g.

    ClassyFireAPI.submit_query_input_in_chunks("input.tsv")

where `input.tsv` is a tab-separated file, and each line must contain either

* Only a structural representation: SMILES, InChI, or
* A tab-separated pair of an ID and the corresponding structure representation: SMILES, InChI

To  submit multiple IUPAC name from a file, use e.g.

    ClassyFireAPI.submit_query_input_in_chunks("input.tsv", type="IUPAC NAME")

where `input.tsv` is a tab-separated file, and each line must contain either

* Only an IUPAC name, or
* A tab-separated pair of an ID and the corresponding IUPAC name

###GET###

**/queries/{queryId}**

The classification results can be retrieved via a GET method in the JSON or XML format.

    ClassyFireAPI.get_query(443431,"json")

where:

* The query id is 443431,
* The output format is "json" (JSON)

##2. Entity##

Chemical compounds are represented by the Entity model, through which one can access the compound's structure-based classification.

###GET###

**/entities/{entityInchikey}**

A chemical compound's classification can be retrieved in the JSON or XML format. The compound's information is accessed via the InChIkey as show in the following examples. In this example, the entity has the InChIKey=LABTWGUMFABVFG-ONEGZZNKSA-N.

    ClassyFireAPI.get_entity_classification("BDAGIHXWWSANSR-UHFFFAOYSA-N","json")

Where:

* The entity's InChIKey is "BDAGIHXWWSANSR-UHFFFAOYSA-N",
* The output format is "json" (JSON).

##3. Sequence##

Sequences are represented by the Sequence model, through which one can access the compound's structure-based classification.

###GET###

**/sequence/{fingerprint}**

A (FASTA) sequence's classification can be retrieved in the JSON. In this example, the sequence has the fingerprint = "8bf70c23ac100cd2c3955081d8613325"

    ClassyFireAPI.get_sequence_classification("8bf70c23ac100cd2c3955081d8613325","json")

 Where:

* The sequence's fingerprint is "8bf70c23ac100cd2c3955081d8613325",
* The fingerprint is generate by Digest::MD5 from the peptide sequence "APDVQDCPECTLQENPFFSQPGAPILQCMGCCFSRAYPTPLRSKKTMLVQKNVTSESTCCVAKSYNRVTVMGGFKVENHTACHCSTCYYHKS"
* The output format is "json" (JSON).


# **Help and Feedback** #

Feel free to send us any questions of feedback [here](http://feedback.wishartlab.com/?site=classyfire).
